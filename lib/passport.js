var TwitterStrategy  = require('passport-twitter').Strategy;
//var twitter = require('twitter');
var Twitter = require('mtwitter');
var pg = require('pg');
pg.defaults.ssl = false;
var pgClient = new pg.Client(process.env.DATABASE_URL);

var config = require('./config');

module.exports = function(passport) {

	passport.serializeUser(function(user, done) {
        done(null, user);
    });

	passport.deserializeUser(function(obj, done) {
        done(null, obj);
    });

    passport.use(new TwitterStrategy({

        consumerKey     : config.twitterAuth.consumerKey,
        consumerSecret  : config.twitterAuth.consumerSecret,
        callbackURL     : config.twitterAuth.callbackURL,
        userAuthorizationURL: 'https://api.twitter.com/oauth/authorize'
    },
    function(token, tokenSecret, profile, done) {
        pgClient.connect();
        pgClient.query("SELECT * from users where twitter_id=($1)", [profile.id] , function (err, result){
            if(err){
                pgClient.end();
                return;
            }
            if(result.rows && result.rows.length> 0)
            {
                var query = pgClient.query("UPDATE users SET twitter_token=($1), twitter_token_secret=($2) WHERE twitter_id=($3)", [token, tokenSecret, profile.id]);
            }
            else
            {
                var query = pgClient.query("INSERT INTO users(twitter_id, twitter_token, twitter_token_secret) values($1, $2, $3)", [profile.id, token, tokenSecret]);
            }
            query.on('end', function() {
                pgClient.end();
            });
        });
        return done(null, profile);
    }));
}