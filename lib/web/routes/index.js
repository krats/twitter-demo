var uuid = require('node-uuid');
var fs = require('fs');
const aws = require('aws-sdk');
const S3_BUCKET = process.env.S3_BUCKET_NAME;
var pg = require('pg');
pg.defaults.ssl = false;
var pgClient = new pg.Client(process.env.DATABASE_URL);


module.exports = function(app, isLoggedIn) {

	app.get('/', isLoggedIn, function(req, res){
		res.render('index.ejs');
	});

	app.post('/', isLoggedIn, function(req, res){
		if(!req.body.fileurl)
		{
			var e = new Error('Invalid file url');
    		e.status = 500;
			res.send(e);
			return;
		}
		if(!req.body.postdate)
		{
			var e = new Error('Invalid post date');
    		e.status = 500;
			res.send(e);
			return;
		}
		if(!req.body.posttime)
		{
			var e = new Error('Invalid post time');
    		e.status = 500;
			res.send(e);
			return;
		}
		var url = req.body.fileurl;
		var date = req.body.postdate;
		var time = req.body.posttime;
		var offset = parseInt(req.body.timezoneoffset);
		if(!offset){
			offset = 0;
		}
		var server_offset = (new Date()).getTimezoneOffset();
		offset -= server_offset;
		var date_components = date.split('-');
		var time_components = time.split(':');
		var datetime = new Date((new Date(parseInt(date_components[0]), parseInt(date_components[1]) - 1, parseInt(date_components[2]), parseInt(time_components[0]), parseInt(time_components[1]), 0, 0)).getTime() + offset*60*1000);
		pgClient.connect();
		var query = pgClient.query("INSERT INTO tasks(twitter_id, s3_url, upload_timestamp) values($1, $2, $3)", [req.user.id, url, datetime]);
        query.on('end', function(){
            pgClient.end();
        });
        res.redirect('/');
	});

	app.get('/sign-s3', isLoggedIn, function(req, res){
		const s3 = new aws.S3();
		const fileName = req.query['file-name'];
		var parts = fileName.split('.');
		var extension = parts[parts.length-1];
		var name = "img-" + uuid.v1() + "." + extension;
		const fileType = req.query['file-type'];
		const s3Params = {
			Bucket: S3_BUCKET,
			Key: name,
			Expires: 60,
			ContentType: fileType,
			ACL: 'public-read'
		};

		s3.getSignedUrl('putObject', s3Params, function(err, data){
			if(err){
				console.log(err);
				return res.end();
			}
			const returnData = {
				signedRequest: data,
				url: `https://${S3_BUCKET}.s3.amazonaws.com/${name}`
			};
			res.write(JSON.stringify(returnData));
			res.end();
		});
	});
}