module.exports = function(app, passport) {

	app.get('/login/twitter', passport.authenticate('twitter'));

	app.get('/login/twitter/callback', function(req, res, next){
		next();
	},
    passport.authenticate('twitter', {
        successRedirect : '/',
        failureRedirect : '/login'
    }));
}