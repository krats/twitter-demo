var twitter = require('./twitter');
module.exports = function(app, passport, isLoggedIn) {

	app.get('/login', function(req,res){
		res.render('login');
	});

	app.get('/logout', isLoggedIn, function(req,res){
		req.logout();
        req.session.destroy();
        res.redirect('/');
	});

	twitter(app, passport);
	
}