var express  = require('express');
var passport = require('passport');
var flash    = require('connect-flash');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');
var favicon = require('serve-favicon');

var login_routes = require('./routes/login.js');
var index_routes = require('./routes/index.js');

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/login');
}

var app      = express();
app.set('port', (process.env.PORT || 3000));
app.use(favicon(__dirname + '/public/images/favicon.ico'));
app.use(express.static(__dirname + '/public'));
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

require('../passport')(passport);
app.use(session({ secret: 'ilovescotchscotchyscotchscotch', resave: true, saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

login_routes(app, passport, isLoggedIn);
index_routes(app, isLoggedIn);

app.listen(app.get('port'), function() {
 	console.log('Node app is running on port', app.get('port'));
});