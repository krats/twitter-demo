var fs = require('fs');
var https = require('https');

module.exports = function(url, filename){
	return new Promise(function(resolve, reject){
		https.get(url, function(response) {
		    var writeStream = fs.createWriteStream(filename);
		    response.pipe(writeStream);
		    writeStream.on('finish', function(){
		    	writeStream.close(resolve);
		    });
  		});
	});
};