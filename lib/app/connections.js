var postgres = require('pg').Client;
var jackrabbit = require('jackrabbit');
var logger = require('logfmt');
var EventEmitter = require('events').EventEmitter;

function Connector(postgresUrl, rabbitUrl) {
  EventEmitter.call(this);

  var self = this;
  var readyCount = 0;

  // this.db = new postgres(postgresUrl)
  // this.db.connect();
  // logger.log({ type: 'info', msg: 'connected', service: 'postgres' });
  // ready();

  // this.db
  //   .on('error', function(err) {
  //     logger.log({ type: 'error', msg: err, service: 'mongodb' });
  //     lost();
  //   })
  //   .on('close', function(str) {
  //     logger.log({ type: 'error', msg: 'closed', service: 'mongodb' });
  //     lost();
  //   })
  //   .on('end', function() {
  //     logger.log({ type: 'error', msg: 'disconnected', service: 'mongodb' });
  //     lost();
  //   });

  this.queue = jackrabbit(rabbitUrl)
    .on('connected', function() {
      logger.log({ type: 'info', msg: 'connected', service: 'rabbitmq' });
      ready();
    })
    .on('error', function(err) {
      logger.log({ type: 'error', msg: err, service: 'rabbitmq' });
    })
    .on('disconnected', function() {
      logger.log({ type: 'error', msg: 'disconnected', service: 'rabbitmq' });
      //lost();
    });

  function ready() {
    if (++readyCount === 1) {
      self.emit('ready');
    }
  }

  function lost() {
    self.emit('lost');
  }
};

Connector.prototype = Object.create(EventEmitter.prototype);

module.exports = function(postgresUrl, rabbitUrl) {
  return new Connector(postgresUrl, rabbitUrl);
};
