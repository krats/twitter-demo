var Twitter = require('twit');
var config = require('../config');

module.exports = function(access_token, access_token_secret){
	var options = {
		consumer_key:         config.twitterAuth.consumerKey,
		consumer_secret:      config.twitterAuth.consumerSecret,
		access_token:         access_token,
		access_token_secret:  access_token_secret,
		timeout_ms:           60*1000
	};
	return new Twitter(options);
};