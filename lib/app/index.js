var logger = require('logfmt');
var Promise = require('promise');
var fs = require('fs');
var EventEmitter = require('events').EventEmitter;

var connections = require('./connections');
var twitter = require('./twitter');
var download = require('./request');

var UPLOAD_QUEUE = 'jobs.upload';

function App(config) {
  EventEmitter.call(this);

  this.config = config;
  this.connections = connections(config.POSTGRES_URL, config.RABBIT_URL);
  this.connections.once('ready', this.onConnected.bind(this));
  this.connections.once('lost', this.onLost.bind(this));
}

module.exports = function createApp(config) {
  return new App(config);
};

App.prototype = Object.create(EventEmitter.prototype);

App.prototype.onConnected = function() {
  var queues = 0;

  this.connections.queue.create(UPLOAD_QUEUE, { prefetch: 5 }, onCreate.bind(this));
  function onCreate() {
    if (++queues === 1) this.onReady();
  }
};

App.prototype.onReady = function() {
  logger.log({ type: 'info', msg: 'app.ready' });
  this.emit('ready');
};

App.prototype.onLost = function() {
  logger.log({ type: 'info', msg: 'app.lost' });
  this.emit('lost');
};

App.prototype.addTask = function(userId, url, token, tokenSecret) {
  var id = uuid.v1();
  this.connections.queue.publish(UPLOAD_QUEUE, { id: id, url: url, userId: userId , token: token, tokenSecret: tokenSecret});
  return Promise.resolve(id);
};

App.prototype.startUploading = function() {
  this.connections.queue.handle(UPLOAD_QUEUE, this.handleUploadJob.bind(this));
  return this;
};

App.prototype.handleUploadJob = function(job, ack) {
  logger.log({ type: 'info', msg: 'handling job', queue: UPLOAD_QUEUE, url: job.url });

  var that = this;
  job.retryCount = 5;

  var url_components = job.url.split('.');
  var filename = 'img-'+ job.id + '.' + url_components[url_components.length - 1];
  var rp1 = download(job.url, filename);
  var that = this;
  var on_image_download = function(){
    that
    .UploadImage(job.userId, job.id, job.url, job.token, job.tokenSecret, filename)
    .then(onSuccess, onError);

    function onSuccess() {
      logger.log({ type: 'info', msg: 'job complete', status: 'success' });
      fs.unlinkSync(filename);
      ack();
    }

    function onError(err) {
      logger.log({ type: 'info', msg: 'job complete', status: 'failure' , err: err});
      job.retryCount--;
      if(job.retryCount > 0)
      {
        setTimeout(function(){
          that.UploadImage(job.userId, job.id, job.url, job.token, job.tokenSecret, filename)
            .then(onSuccess, onError);
        }, 5000);
      }
      else
      {
        ack();
      }
    }
  }
  rp1.then(on_image_download);
};

App.prototype.UploadImage = function(userId, id, url, token, tokenSecret, filename) {
  var twitterClient = twitter(token, tokenSecret);
  return new Promise(function(resolve, reject){
    var b64content = fs.readFileSync(filename, { encoding: 'base64' });
  	twitterClient.post('media/upload', { media_data: b64content }, function(err, data, response) {
  	  if(err) reject(err);
      twitterClient.post('/statuses/update', { status: url , media_ids: [data.media_id_string] }, function(err, data, response) {
        if(err) reject(err);
        resolve();
     });
	 });
  });
};

App.prototype.stopUploading = function() {
  this.connections.queue.ignore(UPLOAD_QUEUE);
  return this;
};

