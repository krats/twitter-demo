var http = require('http');
var logger = require('logfmt');
var throng = require('throng');

var config = require('./config');
var app = require('./app');

http.globalAgent.maxSockets = Infinity;
throng({
  workers: config.workerConcurrency,
  start: start
});

function start() {
  logger.log({
    type: 'info',
    msg: 'starting worker',
    concurrency: config.workerConcurrency
  });

  var instance = app(config);

  instance.on('ready', beginWork);
  process.on('SIGTERM', shutdown);

  function beginWork() {
    instance.on('lost', shutdown);
    instance.startUploading();
  }

  function shutdown() {
    logger.log({ type: 'info', msg: 'shutting down' });
    process.exit();
  }
}