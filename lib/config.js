module.exports = {
    twitterAuth : {
        consumerKey       : process.env.consumerKey,
        consumerSecret    : process.env.consumerSecret,
        callbackURL       : process.env.callbackURL 
    },
    POSTGRES_URL : process.env.DATABASE_URL || 'postgres://localhost:5432/twitteruploaddemo',
    RABBIT_URL: process.env.CLOUDAMQP_URL || 'amqp://localhost',
    S3_BUCKET_NAME : process.env.S3_BUCKET_NAME,
    AWS_SECRET_ACCESS_KEY: process.env.AWS_SECRET_ACCESS_KEY,
    AWS_ACCESS_KEY_ID: process.env.AWS_ACCESS_KEY_ID,
    workerConcurrency: process.env.WORKER_CONCURRENCY || 1
}